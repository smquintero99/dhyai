// https://medium.com/react-native-training/react-native-animations-using-the-animated-api-ebe8e0669fae
// https://kmagiera.github.io/react-native-gesture-handler/docs/getting-started.html
// https://www.npmjs.com/package/react-native-numeric-input
// https://github.com/oblador/react-native-progress

import { StyleSheet, View, Animated, TouchableOpacity, Text } from 'react-native'
import {PanGestureHandler, Directions, State} from 'react-native-gesture-handler'
import { createStackNavigator, createAppContainer } from "react-navigation"
import NumericInput from 'react-native-numeric-input'
import { ListItem } from 'react-native-elements'
import { GraphQLClient } from 'graphql-request'
import { Bar } from 'react-native-progress'
import moment from 'moment'
import React from 'react'

const client = new GraphQLClient('https://api.graph.cool/simple/v1/cjv9g6gcu0s1e0110k1tb4c4e', {})

class Home extends React.Component {

  static navigationOptions = {
    header: null
  }

  constructor () {
    super()
    this.springValue = new Animated.Value(1)
    this.state = {
      start: false,
      progress: 0,
      timer:10,
      inhalations:0,
      inhalated:0,
      exhalations:0,
      exhalated:0,
      last_date: null
    }
  }

  start = () => {
    this.setState({start:true, last_date: new Date(), progress:0})
    interval = setInterval(() => {
      this.state.progress > 1 
      ? this.stop()
      : this.setState({progress:this.state.progress + 0.01 })
    }, 60*1000*this.state.timer/100);
  }

  stop = () => {
    clearInterval(interval)
    new Date() - this.state.last_date < 60000
      ? this.state.inhalations > this.state.exhalations
        ? this.setState({ 
            exhalations: this.state.exhalations + 1, 
            exhalated: this.state.exhalated + (new Date() - this.state.last_date),
            start: false
          })
        : this.setState({
            inhalations: this.state.inhalations + 1, 
            inhalated: this.state.inhalated + (new Date() - this.state.last_date),
            start: false
          })    
      : null
    setTimeout(() => {
      client.request(`mutation
        {
          createMeditation(
            meditated:${(this.state.inhalated + this.state.exhalated)/60000}
            inhalated:${this.state.inhalated/(this.state.inhalations*1000)}
            exhalated:${this.state.exhalated/(this.state.exhalations*1000)}
          ){
            id
            createdAt
            meditated
            inhalated
            exhalated
          }
        }
      `).then(console.log)
    }, 1000)
  }

  spring (){
    !this.state.start 
      ? this.start() 
      : this.setState({
        exhalations: this.state.exhalations + 1, 
        exhalated: this.state.exhalated + (new Date() - this.state.last_date),
        last_date: new Date()
      })
    Animated.timing(
      this.springValue,
      {
        duration: 5000,
        toValue: 1.5
      }
    ).start()
  }

  contract () {
    this.setState({
      inhalations: this.state.inhalations + 1, 
      inhalated: this.state.inhalated + (new Date() - this.state.last_date),
      last_date: new Date()
    })
    Animated.timing(
      this.springValue,
      {
        duration: 3000,
        toValue: 1
      }
    ).start()
  }
  
  render() {
    return (
      <PanGestureHandler
        minDist={250}
        direction={Directions.RIGHT}
        onHandlerStateChange={({ nativeEvent }) => {
          if (nativeEvent.state === State.ACTIVE) {
            this.props.navigation.navigate('Details')
          }
        }}
      >
        <View style={styles.container}>

          <Bar 
            animated={false} 
            progress={this.state.progress} 
            width={300} 
            style={{marginBottom:80}} 
            color={'#00D8FF'}
            borderColor={'#00D8FF'}
          />

          <TouchableOpacity
            activeOpacity = {1}
            onPressIn = {() => this.spring()}
            onPressOut = {() => this.contract()}
          >
            <Animated.Image
              style={{ width: 227, height: 200, transform: [{scale: this.springValue}] }}
              source={require('./assets/reactjs.png')}
            />
          </TouchableOpacity>

          <NumericInput 
            totalHeight={50} 
            iconSize = {45}
            onChange = {timer => this.setState({timer})}
            containerStyle = {{marginTop:80}}
            textColor = '#00D8FF'
            borderColor = '#00D8FF'
            iconStyle = {{ color: '#00D8FF' }} 
            initValue = {this.state.timer}
            minValue = {1}
          />
        </View>
      </PanGestureHandler>
    )
  }
}

class DetailsScreen extends React.Component {
  constructor () {
    super()
    this.state = {
      meditations:[]
    }
  }

  static navigationOptions = {
    title: 'Diario de Meditación',
  }

  componentDidMount(){
    return client.request(`
      {
        allMeditations(
          orderBy: createdAt_DESC
        ){
          createdAt
          inhalated
          exhalated
          meditated
        }
      }
    `).then(({allMeditations}) => this.setState({meditations:allMeditations}))
  }

  render() {
    return (
      <View style={{justifyContent: 'center'}}>
        {
          this.state.meditations.map((meditation, key) => (
            <ListItem
              key={key}
              leftElement={
                <Text style={{color:"#00D8FF"}}>
                  <Text style={{fontSize:22, fontWeight:'bold', marginLeft:10}}>{Math.round(meditation.meditated*10)/10}</Text> Min. {"\n"}
                  <Text style={{color:"#00D8FF"}}>Meditación</Text>
                </Text>
              }
              title={
                <View style={{flexDirection:'row', flexWrap:'wrap'}}>
                  <Text style={{color:"#00D8FF"}}>
                    <Text style={{fontSize:20, fontWeight:'bold'}}>{Math.round(meditation.inhalated*10)/10}</Text> Sec. {"\n"}
                    <Text>Inhalación</Text>
                  </Text>
                  <Text style={{color:"#00D8FF", marginLeft:15}}>
                   <Text style={{fontSize:20, fontWeight:'bold'}}>{Math.round(meditation.exhalated*10)/10}</Text> Sec{"\n"}
                    <Text>Exhalación</Text>
                  </Text>
                </View>
              }
              rightTitle={moment(meditation.createdAt).fromNow()}
              rightTitleStyle={{color:"#00D8FF", fontSize:13}}
            />
          ))
        }
      </View>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    Home: Home,
    Details: DetailsScreen
  }, 
  {
    initialRouteName: "Home"    
  }
);

export default createAppContainer(AppNavigator)
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10
  },
})
